import logo from './logo.svg';
import './App.css';
import ChatContent from './component/ChatContent';

function App() {
  return (
    <div className="main">
      <ChatContent />
    </div>
  );
}

export default App;
