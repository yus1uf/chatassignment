import React, { Component, useState, createRef, useEffect } from "react";

import "./chatContent.css";
import Avatar from "./Avatar";
import ChatItem from "./ChatItem";
import logo from "./agent.png"
import send from "./send.png"

export default class ChatContent extends Component {
  messagesEndRef = createRef(null);
  chatItms = [
    {
      msg: "Hi User",
      createdAt: "2021-09-14T13:23:02.298Z",
      type: "other",
      _id: 1,
      image:{logo},
    },
    {
      msg: "Hi",
      createdAt: "2021-09-14T13:23:02.298Z",
      type: "user",
      _id: 2,
      image:{logo},
    },
    {
      msg: "How are you doing today",
      createdAt: "2021-09-14T13:23:02.298Z",
      type: "other",
      _id: 3,
      image:{logo},
    },
    {
      msg: "I am good, thanks for asking",
      createdAt: "2021-09-14T13:23:02.298Z",
      type: "user",
      _id: 4,
      image:{logo},
    },
    {
      msg: "How can I help you today ?",
      createdAt: "2021-09-14T13:23:02.298Z",
      type: "other",
      _id: 5,
      image:{logo},
    },

  ];

  constructor(props) {
    super(props);
    this.state = {
      chat: this.chatItms,
      msg: "",
    };
    this.addData=this.addData.bind(this);
  }

  scrollToBottom = () => {
    this.messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

       addData() {
        if (this.state.msg != "") {
          this.chatItms.push({
            msg: this.state.msg,
            createdAt: new Date(),
            type: "",
            _id: this.chatItms.length+1,
            image:{logo},
          });
          this.setState({ chat: [...this.chatItms] });
          this.scrollToBottom();
          this.setState({ msg: "" });
         // this.scrollToBottom();
        }
        this.scrollToBottom();
      }
    
  onStateChange = (e) => {
    this.setState({ msg: e.target.value });
    this.scrollToBottom();
  };

  render() {
    return (
      <div className="main__chatcontent">
        <div className="content__header">
          <div className="blocks">
            <div className="current-chatting-user">
              <Avatar
                isOnline="active"
                image={logo}
              />
              <p>Agent</p>
            </div>
          </div>

          
        </div>
        <div className="content__body">
          <div className="chat__items">
            {this.state.chat.map((itm, index) => {
              return (
                <ChatItem
                  animationDelay={index + 2}
                  key={itm.key}
                  user={itm.type ? itm.type : "me"}
                  msg={itm.msg}
                  createdAt={itm.createdAt}
                  image={logo}
                />
              );
            })}
            <div ref={this.messagesEndRef} />
          </div>
        </div>
        <div className="content__footer">
          <div className="sendNewMessage">
            <input
              type="text"
              placeholder="Type a message here"
              onChange={this.onStateChange}
              value={this.state.msg}
            />
            <button class="btnSendMsg" onClick={this.addData}>
                    <img src={send} alt="Send"/>
                </button>
          </div>
        </div>
      </div>
    );
  }
}
