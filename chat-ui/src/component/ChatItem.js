import React, { Component } from "react";
import Avatar from "./Avatar";
import dayjs from "dayjs";
import relativeTime from 'dayjs/plugin/relativeTime';
export default class ChatItem extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    dayjs.extend(relativeTime);
    return (
      <div
        style={{ animationDelay: `0.8s` }}
        className={`chat__item ${this.props.user ? this.props.user : "agent"}`}
      >
        <div className="chat__item__content">
          <div className="chat__msg">{this.props.msg}</div>
          <div className="chat__meta">
            <span>{dayjs(this.props.createdAt).fromNow()}</span>
          </div>
        </div>
        <Avatar isOnline="active" image={this.props.image} />
      </div>
    
    );
  }
}
